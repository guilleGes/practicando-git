<?php

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document (MASTER)</title>
</head>
<body>
<h1>Estamos en equipo</h1>

<section>
    <h2>Rama master</h2>
    <p>Estoy en la rama Master</p>
    <article>
        <p>Contenido de la <br>
        rama master</p>
    </article>
</section>

<section>
    <h2>Esta es la rama dev</h2>
    <p>Esta rama es la nueva creada para resolución de conflictos</p>
</section>
</body>
</html>
